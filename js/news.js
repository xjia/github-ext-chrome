$(function(){
  function style_by_class_name(class_name){
    return 'github-ext-' + class_name;
  }

  function hide_by_class_name(class_name){
    var k = style_by_class_name(class_name);
    $('<style class="' + k + '" type="text/css">#dashboard .news .' + class_name + '{display:none;}</style>').appendTo('head');
  }

  function show_by_class_name(class_name){
    var k = style_by_class_name(class_name);
    $('style.' + k).remove();
  }

  function hide_news($btn){
    hide_by_class_name($btn.data('class-name'));
    $btn.removeClass('active');
  }

  function show_news($btn){
    show_by_class_name($btn.data('class-name'));
    $btn.addClass('active');
  }

  $.ajax({
    url: chrome.extension.getURL('html/news.html'),
    dataType: 'html',
    success: function(html){
      $('#dashboard .news').prepend(html);
      $('#news-filter button').click(function(){
        if ($(this).hasClass('active')) {
          hide_news($(this));
        } else {
          show_news($(this));
        }
      });
      $('#news-filter button').bind('contextmenu', function(e){
        if ($(this).hasClass('active')) {
          hide_news($(this));
          $('#news-filter button').not($(this)).each(function(){
            show_news($(this));
          });
        } else {
          $('#news-filter button').not($(this)).each(function(){
            hide_news($(this));
          });
          show_news($(this));
        }
        e.preventDefault();
        return false;
      });
    }
  });
});