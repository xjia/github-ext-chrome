github-ext-chrome
=================

Chrome Extension for GitHub UI Enhancement


Option 1: Install from a packaged file
--------------------------------------

1. Download [github-ext-chrome.crx](http://xiao-jia.com/downloads/github-ext-chrome.crx)
2. Chrome => Settings => Extensions
3. **Drag** github-ext-chrome.crx to the extensions window


Option 2: Install from source code
----------------------------------

1. `$ git clone git@github.com:stfairy/github-ext-chrome.git`
2. Chrome => Settings => Extensions
3. Check "Developer mode" on the top right corner
4. Click "Load unpacked extension..."
5. Choose the cloned directory


Usage
-----

1. Navigate to your GitHub dashboard
2. Left click to toggle filters one by one
3. Right click to toggle all the **other** filters

License
-------
              DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                      Version 2, December 2004 

    Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

    Everyone is permitted to copy and distribute verbatim or modified 
    copies of this license document, and changing it is allowed as long 
    as the name is changed. 

              DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
     TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

    0. You just DO WHAT THE FUCK YOU WANT TO. 